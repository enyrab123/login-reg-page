import React, { useEffect, useState } from "react";
import { Button, Container, Form, FormGroup, Modal } from "react-bootstrap";
import { useNavigate } from "react-router-dom";
import Swal from "sweetalert2";

export const HomePage = () => {
  const [info, setInfo] = useState([]);
  const navigate = useNavigate();
  const [firstName, setFirstName] = useState("");
  const [lastName, setLastName] = useState("");
  const [showModal, setShowModal] = useState(false);
  const handleClose = () => setShowModal(false);
  const handleShow = () => setShowModal(true);

  async function changeName(e) {
    // Prevents the page from reloading upon form submission
    e.preventDefault();
    const url = "http://localhost:4000/users/change-name/";

    try {
      const response = await fetch(url, {
        method: "PUT",
        headers: {
          "Content-Type": "application/json",
          Authorization: `Bearer ${localStorage.getItem("token")}`,
        },
        body: JSON.stringify({
          firstName: firstName,
          lastName: lastName,
        }),
      });

      const result = await response.json();

      if (result.status === 201) {
        Swal.fire({
          title: "Change name Success",
          icon: "success",
          text: result.message,
        });

        // Update info state with new name
        setInfo((prevInfo) => ({
          ...prevInfo,
          firstName: firstName,
          lastName: lastName,
        }));

        handleClose();
      } else {
        Swal.fire({
          title: "Change name Failed",
          icon: "error",
          text: result.message,
        });

        handleClose();
      }
    } catch (error) {
      console.error(error);
    }
  } // End of changeName

  const handleLogout = () => {
    localStorage.removeItem("token"); // Clear localStorage

    navigate("/"); // Back to index page
  }; // End of handleLogout

  async function getInfo() {
    const url = "http://localhost:4000/users/retrieve-user-info/";

    try {
      const response = await fetch(url, {
        method: "GET",
        headers: {
          "Content-Type": "application/json",
          Authorization: `Bearer ${localStorage.getItem("token")}`,
        },
      });

      const result = await response.json();

      setInfo(result);
      setFirstName(result.firstName); // Setting first name
      setLastName(result.lastName); // Setting last name
    } catch (error) {
      console.error(error);
    }
  } // End of getInfo

  useEffect(() => {
    getInfo();
  }, []);

  return (
    <Container>
      <h1 className="text-light text-center">
        Hi, Welcome, {info.firstName} {info.lastName}!
      </h1>
      <div className="text-center mt-5">
        <Button className="me-1" onClick={handleShow}>
          Change name
        </Button>
        <Button variant="secondary" onClick={handleLogout}>
          Logout
        </Button>
      </div>

      <Modal show={showModal} onHide={handleClose}>
        <Modal.Header closeButton>
          <Modal.Title>Change name</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Form className="mt-4" onSubmit={(e) => changeName(e)}>
            <FormGroup>
              <Form.Control
                type="text"
                placeholder="First name"
                value={firstName}
                onChange={(e) => setFirstName(e.target.value)}
                required
              />
            </FormGroup>
            <FormGroup className="mt-3">
              <Form.Control
                type="text"
                placeholder="Last name"
                value={lastName}
                onChange={(e) => setLastName(e.target.value)}
                required
              />
            </FormGroup>
          </Form>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={handleClose}>
            Cancel
          </Button>
          <Button variant="primary" onClick={changeName}>
            Save Changes
          </Button>
        </Modal.Footer>
      </Modal>
    </Container>
  );
};
