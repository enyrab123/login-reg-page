import { Card, CardBody, Container } from "react-bootstrap";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import Form from "react-bootstrap/Form";
import FormGroup from "react-bootstrap/FormGroup";
import Button from "react-bootstrap/Button";
import React, { useEffect, useState } from "react";
import { Link, useNavigate } from "react-router-dom";
import Swal from "sweetalert2";

export const LoginPage = () => {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  // Set button to enabled or disabled
  const [isActive, setIsActive] = useState(false);
  const navigate = useNavigate();

  async function authenticate(e) {
    // Prevents the page from reloading upon form submission
    e.preventDefault();

    const url = `http://localhost:4000/users/login`;

    try {
      const response = await fetch(url, {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify({
          email: email,
          password: password,
        }),
      });

      const result = await response.json();

      if (result.access) {
        // Store the access token in the browser's localStorage
        localStorage.setItem("token", result.access);

        // Retrieve user information using access token and user ID
        retrieveUserInfo(result.access, result.id);

        Swal.fire({
          title: "Login Successful",
          icon: "success",
          text: "Welcome user!",
        });

        // Navigate to homepage after successfull login
        navigate("/homepage");
      } else {
        Swal.fire({
          title: "Authentication failed",
          icon: "error",
          text: result.message,
        });
      }
    } catch (error) {
      console.error(error);
    }
  } // End of authenticate

  async function retrieveUserInfo() {
    const url = "http://localhost:4000/users/retrieve-user-info/";

    try {
      const response = await fetch(url, {
        method: "GET",
        headers: {
          "Content-Type": "application/json",
          Authorization: `Bearer ${localStorage.getItem("token")}`,
        },
      });

      const result = await response.json();

      console.log(result);
    } catch (error) {
      console.error(error);
    }
  } // End of retrieveUserInfo

  // Set button disabled if fields are empty
  const disableBtn = () => {
    if (email !== "" && password !== "") {
      setIsActive(true);
    } else {
      setIsActive(false);
    }
  }; // End of setButton

  useEffect(() => {
    disableBtn();
  }, [email, password]);

  return (
    <Container fluid className="p-5">
      <Row className="justify-content-center">
        <Col className="col-sm-12 col-lg-4 col-xl-3">
          <Card className="shadow" style={{ borderColor: "#404040" }}>
            <CardBody style={{ backgroundColor: "#404040" }}>
              <div className="p-3" style={{ backgroundColor: "#404040" }}>
                <h1 className=" text-light fw-bold">
                  Stay connected.
                  <span className="text-primary"> Login to your account!</span>
                </h1>
                <Form className="mt-4" onSubmit={(e) => authenticate(e)}>
                  <FormGroup>
                    <Form.Control
                      className="dark-mode-input"
                      type="email"
                      placeholder="Email address"
                      value={email}
                      onChange={(e) => setEmail(e.target.value)}
                      required
                    />
                  </FormGroup>
                  <FormGroup className="mt-3">
                    <Form.Control
                      className="dark-mode-input"
                      type="password"
                      placeholder="Password"
                      value={password}
                      onChange={(e) => setPassword(e.target.value)}
                      required
                    />
                  </FormGroup>
                  <div className="d-grid mt-3">
                    {isActive ? (
                      <Button type="submit" id="submitBtn">
                        Log in
                      </Button>
                    ) : (
                      <Button disabled type="submit" id="submitBtn">
                        Log in
                      </Button>
                    )}
                  </div>
                </Form>

                <p className="mt-4 mb-0 text-light small text-center">
                  No account yet? <Link to="/register">Register here.</Link>
                </p>
              </div>
            </CardBody>
          </Card>
        </Col>
      </Row>
    </Container>
  );
};
