import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import { Button, Card, CardBody, Form, FormGroup } from "react-bootstrap";
import React, { useEffect, useState } from "react";
import { Link, useNavigate } from "react-router-dom";
import Swal from "sweetalert2";

export const RegisterPage = () => {
  const [firstName, setFirstName] = useState("");
  const [lastName, setLastName] = useState("");
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [cpassword, setCpassword] = useState("");
  // Set button to enabled or disabled
  const [isActive, setIsActive] = useState(false);
  const navigate = useNavigate();

  async function register(e) {
    const url = "http://localhost:4000/users/registration";

    try {
      // Prevents the page from reloading upon form submission
      e.preventDefault();

      const response = await fetch(url, {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify({
          firstName: firstName,
          lastName: lastName,
          email: email,
          password: password,
        }),
      });

      const result = await response.json();

      if (result.status === 201) {
        Swal.fire({
          title: "Register Successful",
          icon: "success",
          text: result.message,
        });

        // Navigate to index
        navigate("/");
      } else {
        Swal.fire({
          title: "Register Failed",
          icon: "error",
          text: result.message,
        });
      }
    } catch (error) {
      console.error(error);
    }
  } // End of register

  // Set button disabled if fields are empty
  const disableBtn = () => {
    if (
      firstName !== "" &&
      lastName !== "" &&
      email !== "" &&
      password !== "" &&
      cpassword !== "" &&
      password === cpassword
    ) {
      setIsActive(true);
    } else {
      setIsActive(false);
    }
  }; // End of setButton

  useEffect(() => {
    disableBtn();
  }, [firstName, lastName, email, password, cpassword]);

  return (
    <Container fluid className="p-5">
      <Row className="justify-content-center">
        <Col className="col-sm-12 col-lg-4 col-xl-3">
          <Card className="shadow" style={{ borderColor: "#404040" }}>
            <CardBody style={{ backgroundColor: "#404040" }}>
              <div className="p-3">
                <h1 className=" text-light fw-bold">
                  Start your journey.
                  <br />
                  <span className="text-primary"> Register now!</span>
                </h1>
                <Form className="mt-4" onSubmit={(e) => register(e)}>
                  <Row>
                    <Col className="col-6">
                      <FormGroup>
                        <Form.Control
                          className="dark-mode-input"
                          type="text"
                          placeholder="First name"
                          required
                          onChange={(e) => setFirstName(e.target.value)}
                        />
                      </FormGroup>
                    </Col>

                    <Col className="col-6">
                      <FormGroup>
                        <Form.Control
                          className="dark-mode-input"
                          type="text"
                          placeholder="Last name"
                          required
                          onChange={(e) => setLastName(e.target.value)}
                        />
                      </FormGroup>
                    </Col>
                  </Row>

                  <FormGroup className="mt-3">
                    <Form.Control
                      className="dark-mode-input"
                      type="email"
                      placeholder="Email address"
                      onChange={(e) => setEmail(e.target.value)}
                    />
                  </FormGroup>

                  <FormGroup className="mt-3">
                    <Form.Control
                      className="dark-mode-input"
                      type="password"
                      placeholder="Password"
                      required
                      onChange={(e) => setPassword(e.target.value)}
                    />
                  </FormGroup>
                  <FormGroup className="mt-3">
                    <Form.Control
                      className="dark-mode-input"
                      type="password"
                      placeholder="Confirm password"
                      required
                      onChange={(e) => setCpassword(e.target.value)}
                    />
                  </FormGroup>

                  <div className="d-grid mt-3">
                    {isActive ? (
                      <Button type="submit" id="submitBtn">
                        Sign up
                      </Button>
                    ) : (
                      <Button disabled type="submit" id="submitBtn">
                        Sign up
                      </Button>
                    )}
                  </div>
                </Form>

                <p className="mt-4 mb-0 text-light small text-center">
                  Already have an account? <Link to="/">Log in here.</Link>
                </p>
              </div>
            </CardBody>
          </Card>
        </Col>
      </Row>
    </Container>
  );
};
